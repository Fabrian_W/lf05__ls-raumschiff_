package Raumschiff.src;

/**
 * Test-Class for testing purposes only! No other use for it, right now.
 * Test functions in <i>Spaceship</i> and <i>Storage</i>.
 *
 * @author Fabrian
 * @since 18.05.2022 [10:39 AM]
 * @version 1.0
 * */


public class SpaceshipTest {
    /*
    ----------------------------------------
    Notizen zur Aufgabenstellung.

    Aktueller Stand: irgendwo 3 sterne aufgaben??
    Ladung “Photonentorpedos” einsetzen (5 Punkte)

   ----------------------------------------
     */

    public static void main(String[] args) {


        //Klingonen Raumschiff mit Ladung erstellen

        Spaceship klingonen = new Spaceship(1, 100, 100, 100, 100, 2, "IKS Hegh'ta");
        klingonen.addLoadIndex(new Storage("Ferengi Schneckenasft",200));
        klingonen.addLoadIndex(new Storage("Bat'Leth Klingonen Schwert",200));


        //romulaner Raumschiff mit Ladung erstellen

        Spaceship romulaner = new Spaceship(2, 100, 100, 100, 100, 2, "IRW Khazara");
        romulaner.addLoadIndex(new Storage("Borg-Schrott",5));
        romulaner.addLoadIndex(new Storage("Rote Materie",2));
        romulaner.addLoadIndex(new Storage("Plasma-Waffe",50));


        //vulkanier Raumschiff mit Ladung erstellen
        Spaceship vulkanier = new Spaceship(0, 80, 80, 50, 100, 5, "Ni'Var");

        vulkanier.addLoadIndex(new Storage("Forschungssonde",35));
        vulkanier.addLoadIndex(new Storage("Photonentorpedo",3));



        //battle action (excercise 4.2.1)
        klingonen.shootPhotonTorpedo(romulaner, 2); //Die Klingonen schießen mit dem Photonentorpedo einmal auf die Romulaner
        romulaner.shootPhaseCanon(klingonen); //Die Romulaner schießen mit der Phaserkanone zurück.
        vulkanier.messageToAll("Gewalt ist nicht logisch");
        klingonen.spaceshipOutput(); //Zustans des Raumschiffs ausgeben
        klingonen.loadOutput(); //ladung des schiffs ausgeben
        klingonen.shootPhotonTorpedo(romulaner, 1); //Die Klingonen schießen mit dem Photonentorpedo einmal auf die Romulaner

        klingonen.spaceshipOutput(); //Zustans des Raumschiffs ausgeben
        klingonen.loadOutput(); //ladung des schiffs ausgeben

        romulaner.spaceshipOutput(); //Zustans des Raumschiffs ausgeben
        romulaner.loadOutput(); //ladung des schiffs ausgeben

        vulkanier.spaceshipOutput(); //Zustans des Raumschiffs ausgeben
        vulkanier.loadOutput(); //ladung des schiffs ausgeben
        romulaner.broadcastKommunikatorAusgeben();

    }
}
