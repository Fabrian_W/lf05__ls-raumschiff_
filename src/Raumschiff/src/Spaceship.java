package Raumschiff.src;
import java.util.ArrayList;

/**
 * This class creates a spaceship with all of his content like <i>weapons</i> and the spaceship's <i>name</i>.<br>
 * There is an option to add stuff into the <i>Storage</i> as well,<br>
 * there for the class <b>Storage</b> is used, but this methode <b>Storage</b><br>
 * can be called from this class (<b>Spaceship</b>).
 *
 * @author Fabrian
 * @since 19.05.2022 [11:21 AM]
 * @version 1.1
 * */

public class Spaceship {

    private int photonTorpedoAmount;
    private int energyCapacity;
    private int shieldInPercent;
    private int hullInPercent;
    private int LifeSupportSystems;
    private int androidsAmount;
    private String shipName;
    private ArrayList<Storage> loadIndex = new ArrayList<>();
    private ArrayList<String> broadcastKommunikator =  new ArrayList<>();

    //constructor
    public Spaceship() {}
    public Spaceship(int photonTorpedoAmount, int energyCapacity, int shieldInPercent, int hullInPercent,
                     int LifeSupportSystems, int androidsAmount, String shipName) {
        this.photonTorpedoAmount = photonTorpedoAmount;
        this.energyCapacity = energyCapacity;
        this.shieldInPercent = shieldInPercent;
        this.hullInPercent = hullInPercent;
        this.LifeSupportSystems = LifeSupportSystems;
        this.androidsAmount = androidsAmount;
        this.shipName = shipName;
    }


    //getter and setter
    public int getphotonTorpedoAmount() {
        return photonTorpedoAmount;
    }
    public void setphotonTorpedoAmount(int photonTorpedoAmount) {
        this.photonTorpedoAmount = photonTorpedoAmount;
    }
    public int getenergyCapacity() {
        return energyCapacity;
    }
    public void setenergyCapacity(int energyCapacity) {
        this.energyCapacity = energyCapacity;
    }
    public int getshieldInPercent() {
        return shieldInPercent;
    }
    public void setshieldInPercent(int shieldInPercent) {
        this.shieldInPercent = shieldInPercent;
    }
    public int gethullInPercent() {
        return hullInPercent;
    }
    public void sethullInPercent(int hullInPercent) {
        this.hullInPercent = hullInPercent;
    }
    public int getLifeSupportSystems() {
        return LifeSupportSystems;
    }
    public void setLifeSupportSystems(int LifeSupportSystems) {
        this.LifeSupportSystems = LifeSupportSystems;
    }
    public int getandroidsAmount() {
        return androidsAmount;
    }
    public void setandroidsAmount(int androidsAmount) {
        this.androidsAmount = androidsAmount;
    }
    public String getshipName() {
        return shipName;
    }
    public void setshipName(String shipName) {
        this.shipName = shipName;
    }
    public ArrayList<Storage> getloadIndex() {
        return loadIndex;
    }
    public void addLoadIndex(Storage storage) {
        this.loadIndex.add(storage);
    }

    //other methods

    //alles in der Konsole ausgeben

    /**
     * This <i>methode</i> returns a text in the console with <br>
     * the elements of all <i>items</i> in the <i>Storage</i> and <br>
     * their amount.
     */
    
    public void loadOutput(){ //Prints the storage(s) of the spaceship

        System.out.println("\nLadung von " + shipName + ": ");
        for (int i = 0; i<loadIndex.size(); i++){
            System.out.println(loadIndex.get(i).toString()); // loads the storage Item and amount and converts it into string (in class "Storage")
        }
    }

    /**
     * This <i>methode</i> returns a text in the console with <br>
     * the elements of all <i>items</i> with all <i>spaceship stats</i><br>
     * and all <i>items</i> of the <i>spaceship</i> which are not stored in<br>
     * the <i>storage</i>.
     */

    public void spaceshipOutput(){ //console output of all spaceship status

        String ausgabetext = "\nshipName: "   + shipName            +
                "\nPhotonentorpedo Anzahl: "  + photonTorpedoAmount + " Stueck" +
                "\nenergyCapacity: "          + energyCapacity      + " %" +
                "\nSchilde: "                 + shieldInPercent     + " %" +
                "\nHuelle: "                  + hullInPercent       + " %" +
                "\nLebenserhaltungssysteme: " + LifeSupportSystems  + " %"+
                "\nAndroiden: "               + androidsAmount      + " Stueck";

        System.out.println(ausgabetext);
    }

    /**
     * Shoot a Photon-Torpedo to a <i><b>target</b></i> <i>spaceship</i>.
     * If there is no Photon-Torpedo left, a massage for all appears in the console <br>
     * if there is a hit, too.
     * @param spaceship the target <i>spaceship</i> objekt
     * @param shots the amount of shots which are fired
     */
    public void shootPhotonTorpedo(Spaceship spaceship, int shots) { //r is the target spaceship

        if(photonTorpedoAmount <= 0) { //If torpedos are empty -> "click" in console
              Storage photonentorpedo = getLoad("Photonentorpedo");

              if( photonentorpedo == null) {
                  System.out.println("Keine Photonentorpedos gefunden!");
                  messageToAll("-=*Click*=-");
              }
              else{//reload
                  photonTorpedoAmount = photonentorpedo.getAmount();
                  photonentorpedo.setAmount(0);
                  setLoad(photonentorpedo); //das objekt was gesucht wir (soll auf 0 gesetzt werden (amount))
                  cleanUpShipStorage(); //removes all empty items
              }
         }
        else{//shoot all existing torpedos
            if(shots <= photonTorpedoAmount ) {
                messageToAll("(" + shots + ") Photonentorpedo(s) eingesetzt");
                photonTorpedoAmount -= shots;
                do {
                    hit(spaceship); //hit aufrufen und das getroffene Schiff übergeben
                    shots--;
                }
                while (shots > 0);
            }
            else{
                messageToAll("(" + photonTorpedoAmount + ") Photonentorpedo(s) eingesetzt");
                do {
                    hit(spaceship); //hit aufrufen und das getroffene Schiff übergeben
                    photonTorpedoAmount--;
                }
                while (photonTorpedoAmount > 0);
            }
        }

    }

    /**
     * Shoot a Phase-Caon to a <i><b>target</b></i> <i>spaceship</i>.
     * If there is not enought energy left, a massage for all appears in the console<br>
     * if there is a hit, too.
     * @param spaceship the target <i>spaceship</i> objekt
     */

    public void shootPhaseCanon(Spaceship spaceship) { //r is the target spaceship

        if(energyCapacity < 50) { //If energy is less than 50% -> "click" in console
            messageToAll("-=*Click*=-");
        }
        else{
            messageToAll("Phaserkanone abgeschossen");
            energyCapacity -= 50;    //reduction of energyCapacity of 50%
            hit(spaceship); //call "hit" and pass the hit spaceship (r)
        }

    }

    /**
     * Send message to all, which spaceship was hit.
     * reduced the shields by 50%
     * if shields are down, reduce energy and hull by 50%
     * if hull is down -> live system are destroyed + message to all
     * @param spaceship the spaceship which was hit
     * */

    private void hit(Spaceship spaceship) {

        messageToAll("--[" + spaceship.getshipName() + "]-- wurde getroffen");

        // from here on, the calculation of the damage begins
        int shield = spaceship.getshieldInPercent();
        int energy = spaceship.getenergyCapacity();
        int hull = spaceship.gethullInPercent();


        if(shield <=50){
            spaceship.setshieldInPercent(0); //set the shield to 0

            //reduce energy and hull by 50
            if(energy<=50){
                spaceship.setenergyCapacity(0);
            }
            else{
                spaceship.setenergyCapacity(energy-50); //reduce energy by 50%
            }
            if(hull<=50){
                spaceship.sethullInPercent(0);
                spaceship.setLifeSupportSystems(0);
                messageToAll("Alle Lebenserhaltungssysteme wurden vernichtet");
            }
            else{
                spaceship.sethullInPercent(hull-50); //reduce hull by 50%
            }

        }
        else{
            spaceship.setshieldInPercent(shield-50); //reduce shield ny 50%
        }

    }

    /**
     * Send a message to all (<i>console</i>), like a broadcast message and store it in ship log
     * @param message the message which should be sent and stored in ship-log, in <b>String</b> form
     */

    public void messageToAll(String message){

        String newMessage = "[" + shipName + "]: " + message; //edit message to display spaceship name infront of message
        System.out.println(newMessage); //Console output of the message
        this.broadcastKommunikator.add(message); //save message in  ship log for broadcast messages
    }

    /**
     * Returns the broadcast Log of the ship. In the logs all broadcast messages which where send by this ship<br>
     * are returned to the console
     */

    public void broadcastKommunikatorAusgeben(){

        System.out.println("\nBroadcast Kommunikator von [" + shipName + "]\n");
        if(broadcastKommunikator.size()>1) {
            for (int i = 0; i < broadcastKommunikator.size(); i++) {
                System.out.println(broadcastKommunikator.get(i)); // loads each Message and converts it into string
            }
        }
        else {
            System.out.println("---Kein Log verfuegbar---\n");
        }
    }

    /**
     * Send an amount of drones (if <b>drones</b> is bigger than <i>androidsAmount</i>, all available drones are used <br>
     * to repair the <i>hullInPercent</i> (hull) of the spaceship.
     * @param repairDrones the amount of drones which should be used to repair the spaceships <i>hull</i>
     */
    public void repairSpaceship(int repairDrones){

        repairDrones = Math.abs(repairDrones); //generates a positive number, which is needed later for calculation

        if(repairDrones > this.androidsAmount){//use all available drones.
            shieldInPercent +=  repairSpaceshipFormular(androidsAmount); //add the amount of the repaired structure points to the spaceship hull
            androidsAmount = 0; //remove all drones, because the where all used
        }
        else{
            shieldInPercent += repairSpaceshipFormular(repairDrones);
            androidsAmount -= repairDrones; //remove the amount of used drones
        }

    }

    //private section

    /**
     * In here is the formular for the calculation of added structure points for the hull. <br>
     * The number is partly randomly generated.
     * (<i>Randomnumber</i> * |<i>Number of drones used|</i>) / |<i>spaceship-hull structure points</i>|
     * @param repairDrones //the <i>positive</i> amount of drones, which are used to repair the ship
     * @return the points, which can be added to the structure points of the spaceships hull
     */
    private int repairSpaceshipFormular(int repairDrones){
        int randomNumber = (int)(Math.random() *    101);
        int calculatedPercent = (randomNumber * repairDrones) / Math.abs(shieldInPercent);  //The amount which will be added to the leftHP of the spaceship
                                                                                            //Math.abs() generates a positiv number
        return calculatedPercent;
    }

    /**
     * Looking for an object (you want)  in the storage of the spaceship.
     *
     * @param Ladung The name of the wanted object
     * @return Return the Storage object which is loaded with the object, which was requested, if not in storage <i>null</i> is returned
     */
    private Storage getLoad(String Ladung) {
        Storage photonenTorpedos = null;
        for (int i = 0; i<loadIndex.size(); i++){
            if(loadIndex.get(i).getDescription() == Ladung){
                photonenTorpedos = loadIndex.get(i);
                i = loadIndex.size(); //stop the loop immediately
            }
        }
        if(photonenTorpedos != null)
            return photonenTorpedos;
        else
            return null;       //if not in the storage return null
    }

    /**
     * If you changed the amount of an item int the Storage, this methode will replace the old, by the new item
     *
     * @param item //item which was taken before from storage
     */
    private void setLoad(Storage item){
        String itemName = item.getDescription();

        for (int i = 0; i<loadIndex.size(); i++){
            if(loadIndex.get(i).getDescription() == itemName){
                loadIndex.remove(i);
                loadIndex.add(item);
                i = loadIndex.size(); //stop the loop immediately
            }
        }
    }

    /**
     * Clean up of spaceship storage
     * Will remove all Items with amount of 0
     */
    private void cleanUpShipStorage(){
        for (int i = 0; i<loadIndex.size(); i++){
            if(loadIndex.get(i).getAmount() < 0) {
                loadIndex.remove(i);
            }
        }
    }

}
