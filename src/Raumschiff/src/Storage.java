package Raumschiff.src;

/**
 * In this class <b>one</b> <i>storage space</i> of a <i>spaceship</i> is defined. <br>
 * The class can be generated empty or with content in the form of <i><b>Storage(</b>[name],[amount]<b>)</b></i>;
 * @author Fabrian
 * @since 18.05.2022 [10:39 AM]
 * @version 1.0
 *
 * */

public class Storage {

    private String description;
    private int amount;

    public Storage(){}

    public Storage(String description, int amount) {
        super();
        this.description = description;
        this.amount = amount;
    }

    //getter and setter
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    //other methodes

    /**
     * this Methode is used to send an output to the <br>
     * <i>String</i> with content of the "storage" in form of:<br>
     * Item: <i>name</i><br>
     * Amount: <i>int</i>
     *  @return load A String, which contains the item description and the amount in the form of: <br>"Item: <i>item</i><br>Amount: <i>int</i>"
     * */

   @Override public String toString(){

        String load = "Item: " + description +
                "\nAmount: " + amount;
        return load;
    }

}
